﻿
using System;

namespace Robo_Instagram_Perfil
{
    class Robo_Instagram
    {
        static void Main(string[] args)
        {
            string nomePerfil;

            Console.WriteLine("------------------ Robo que busca Informações do Perfil (Instagram) ------------------");
            Console.WriteLine("Nome do perfil para busca: ");
            nomePerfil = Console.ReadLine();

            var perfilAfetado = Instagram.GetPerfilPorUsuario(nomePerfil);

            if (perfilAfetado == null)
            {
                Console.WriteLine("Perfil Não encontrado!");
                Console.WriteLine();
                Console.WriteLine("Deseja tentar novamente? (S/N)");
                var result = Console.ReadLine();

                if (result.ToUpper().Equals("S"))
                {
                    Console.Clear();
                    var perfilAfetado1 = Instagram.validarPerfil();

                    Console.WriteLine("Nome Perfil: " + perfilAfetado1.UserName);
                    Console.WriteLine("Seguidores: " + perfilAfetado1.seguidores.Replace("Followers", ""));
                    Console.WriteLine("Seguindo" + perfilAfetado1.seguindo.Replace("Followin", ""));
                }
            }

            Console.WriteLine("Nome Perfil: " + perfilAfetado.UserName);
            Console.WriteLine("Seguidores: " + perfilAfetado.seguidores.Replace("Followers", ""));
            Console.WriteLine("Seguindo" + perfilAfetado.seguindo.Replace("Followin", ""));

            Console.ReadKey();
        }

    }
}

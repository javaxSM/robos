﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Robo_Instagram_Perfil
{
    public static class Instagram
    {
        public static Perfil GetPerfilPorUsuario(string nomeUsuario)
        {
            var perfil = new Perfil(nomeUsuario);
            string url = @"https://www.instagram.com/" + nomeUsuario + "/";
            string htmlIstagram;
            using (WebClient webClient = new WebClient())
            {
                webClient.Headers["cookie"] = "security=true";

                try
                {
                    htmlIstagram = webClient.DownloadString(url);
                }
                catch
                {
                    return null;
                }
            }

            HtmlAgilityPack.HtmlDocument html = new HtmlAgilityPack.HtmlDocument();
            html.LoadHtml(htmlIstagram);

            var list = html.DocumentNode.SelectNodes("//meta");

            foreach (var node in list)
            {
                string propriedade = node.GetAttributeValue("property", "");

                if(propriedade == "al:ios:app_name")
                {
                    perfil.IosAppName = node.GetAttributeValue("content", "");
                }
                else if (propriedade == "al:ios:app_store_id")
                {
                    perfil.IosAppId = node.GetAttributeValue("content", "");
                }
                else if (propriedade == "al:ios:url")
                {
                    perfil.IosUrl = node.GetAttributeValue("content", "");
                }
                else if (propriedade == "al:android:app_name")
                {
                    perfil.AndroidAppName = node.GetAttributeValue("content", "");
                }
                else if(propriedade == "al:android:package")
                {
                    perfil.AndroidAppId = node.GetAttributeValue("content", "");
                }
                else if(propriedade == "al:android:url")
                {
                    perfil.AndroidUrl = node.GetAttributeValue("content", "");
                }
                else if (propriedade == "og:image")
                {
                    perfil.Image = node.GetAttributeValue("content", "");
                }
                else if(propriedade == "og:title")
                {
                    perfil.Title = node.GetAttributeValue("content", "");
                }
                else if(propriedade == "og:description")
                {
                    string description;
                    description = node.GetAttributeValue("content", "");
                    perfil.seguidores = description.Substring(0, 13);
                    perfil.seguindo = description.Substring(14, 13);
                }
                else if(propriedade == "og:url")
                {
                    perfil.Url = node.GetAttributeValue("content", "");
                }
                else if(propriedade == "og:description")
                {
                    perfil.seguidores = node.GetAttributeValue("content", "");
                }
            }

            return perfil;
        }

        #region Metodos Auxiliares
        public static Perfil validarPerfil()
        {
            Console.WriteLine("------------------ Robo que busca Informações do Perfil (Instagram) ------------------");
            Console.WriteLine("Nome do perfil para busca: ");
            string nomePerfil = Console.ReadLine();
            var perfil = validar(nomePerfil);

            while(perfil == null)
            {
                Console.WriteLine("Perfil Não encontrado! Espere 5 segundos para digitar novamente.");
                Task.Delay(5000);
                Console.Clear();
                validarPerfil();
            }

            return perfil;
        }

        public static Perfil validar(string perfilNome)
        {
            var per = GetPerfilPorUsuario(perfilNome);

            if (per == null)
                return null;

            return per;
        }
        #endregion
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace Robo_MegaSena_JSON
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Informe um numero do concurso:");
            string numeroConcurso = Console.ReadLine();

            if (string.IsNullOrEmpty(numeroConcurso))
            {
                Console.WriteLine("Concurso invalido ou não encontrado!");
            }

            string url = @"http://loterias.caixa.gov.br/wps/portal/loterias/landing/megasena/!ut/p/a1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbwMPI0sDBxNXAOMwrzCjA0sjIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wNnUwNHfxcnSwBgIDUyhCvA5EawAjxsKckMjDDI9FQE-F4ca/dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/Z7_HGK818G0KO6H80AU71KG7J0072/res/id=buscaResultado/c=cacheLevelPage/=/?timestampAjax=1572191618524&concurso=" + numeroConcurso;
            string html = "";

            using (WebClient webClient = new WebClient())
            {
                webClient.Headers["Cookie"] = "security=true";
                html = webClient.DownloadString(url);
            }

            var converteParaJson = JsonConvert.DeserializeObject<Resultado>(html);

            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Concurso selecionado: " + numeroConcurso);
            Console.WriteLine("------------------------------------------------");

            Console.WriteLine("------------------------------------------------ Resultado da Mega Sena:  ------------------------------------------------");

            Console.WriteLine(converteParaJson.resultadoOrdenado);

            Console.ReadKey();
        }
    }
}

﻿using OpenQA.Selenium;
using System;
using prmToolkit.Configuration;
using prmToolkit.Selenium;
using prmToolkit.Selenium.Enum;
using System.Threading.Tasks;
using System.Threading;

namespace Robo_WhatsApp
{
    class Program
    {
        static void Main(string[] args)
        {

            IWebDriver webDriver = WebDriverFactory.CreateWebDriver(Browser.Chrome, @"C:\Sidney\C# - Robos\Robos\Drive", false);

            try
            {
                Console.WriteLine("Abrindo o Whats...");

                webDriver.LoadPage(TimeSpan.FromSeconds(30), "https://web.whatsapp.com/");
               
                Console.WriteLine("Pegue seu celular para leitura do QR Code!");

                Thread.Sleep(TimeSpan.FromSeconds(30));

                string convite = @"COLOCAR AQUI O LINK DO CONVITE DO GRUPO";

                Console.WriteLine("Boa!! Carregando a pagina de convite....");

                webDriver.LoadPage(TimeSpan.FromSeconds(30), convite);

                var btnJoin = webDriver.FindElement(By.Id("action-button"));

                if(btnJoin.Displayed)
                {
                    btnJoin.Click();

                    Console.WriteLine("Escolhendo opção WEB...");

                    Thread.Sleep(TimeSpan.FromSeconds(3));

                    var escolhaWeb = webDriver.FindElement(By.CssSelector("#fallback_block > div > div > a"));

                    escolhaWeb.Click();

                    Console.WriteLine("Entrando no grupo...");
                    Thread.Sleep(TimeSpan.FromSeconds(30));

                    var btnEntrarGrupo = webDriver.FindElement(By.CssSelector("#app > div > span:nth-child(2) > div > div > div > div > div > div > div._3QNwO > div._1WZqU.PNlAR"));

                    //Thread.Sleep(TimeSpan.FromSeconds(5));

                    btnEntrarGrupo.Click();

                    Console.WriteLine("Escrevedo mensagem..");
                    Thread.Sleep(TimeSpan.FromSeconds(30));


                    var textoMesagem = By.XPath("//*[@id='main']/footer/div[1]/div[2]/div/div[2]");
                    webDriver.SetText(textoMesagem, "Olá!! Eu sou o Robo do sidney e agora são: " + DateTime.Now);
                    webDriver.FindElement(textoMesagem).SendKeys(Keys.Enter);

                   // Thread.Sleep(TimeSpan.FromSeconds(20));

                    var menus = webDriver.FindElements(By.ClassName("rAUz7"));

                    if(menus[5].Displayed)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(5));

                        menus[5].Click();
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(5));

                    var SairopcoesMenu = webDriver.FindElements(By.ClassName("_34D8D"));

                    if (SairopcoesMenu[4].Displayed)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(5));

                        SairopcoesMenu[4].Click();
                    }

                    Thread.Sleep(TimeSpan.FromSeconds(5));

                    var btnSairGrupo = webDriver.FindElement(By.ClassName("PNlAR"));

                    if(btnSairGrupo.Displayed)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(5));

                        btnSairGrupo.Click();
                    }

                    Console.WriteLine("Operação realizadacom sucesso!");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            finally
            {
                webDriver.Close();
            }

            Console.ReadKey();
        }
    }
}

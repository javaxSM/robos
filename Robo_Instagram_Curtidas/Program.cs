﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using prmToolkit.Configuration;
using prmToolkit.Selenium;
using prmToolkit.Selenium.Enum;
using System;
using System.Threading;

namespace Robo_Instagram_Curtidas
{
    class Program
    {
        static void Main(string[] args)
        {
            var usuario = Configuration.GetKeyAppSettings("usuario");
            var senha = Configuration.GetKeyAppSettings("Senha");


            IWebDriver webDriver = WebDriverFactory.CreateWebDriver(Browser.Chrome, @"C:\Sidney\C# - Robos\Robos\Drive", false);

            try
            {
                webDriver.LoadPage(TimeSpan.FromSeconds(30), "https://www.instagram.com/accounts/login/");

                Thread.Sleep(5000);

                //string abrirNovaAba = Keys.Control + "t";
                webDriver.SetText(By.Name("username"), usuario);
                webDriver.SetText(By.Name("password"), senha);

                webDriver.Submit(By.TagName("button"));

                Thread.Sleep(TimeSpan.FromSeconds(5));
                webDriver.LoadPage($@"https://www.instagram.com/{}/");
                webDriver.FindElement(By.XPath("//button[contains(text(), 'Seguir')]")).Click();
            }
            catch (AggregateException ex)
            {
                ex.Handle((x) =>
                {
                    if (x is UnauthorizedAccessException) 
                    {
                        Console.WriteLine("Você não tem permissão para acessar a pagina solicitada.");
                        Console.WriteLine("Favor entrar em contato com o administrador do sistema.");

                        return true;
                    }
                    else if(x is InvalidElementStateException)
                    {
                        Console.WriteLine("Tipo do estado do componente invalido! Verifique e tente novamente!");
                    }
                    else
                    {
                        if(x is InvalidElementStateException)
                        {
                            Console.WriteLine("Tag inválida para operação! Verifique e tente novamente!");
                        }
                    }

                    return false;
                });

                Console.WriteLine(ex.Message.ToString());
            }

            Console.ReadKey();
        }
    }
}

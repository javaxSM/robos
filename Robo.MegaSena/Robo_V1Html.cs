﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace Robo.MegaSena_HTML
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Informe um numero do concurso:");
            string numeroConcurso = Console.ReadLine();

            if(String.IsNullOrEmpty(numeroConcurso))
            {
                numeroConcurso = "2202";
            }

            string url = @"http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp?submeteu=sim&opcao=concurso&txtConcurso=" + numeroConcurso;
            string html = "";

            using (WebClient webClient = new WebClient())
            {
                webClient.Headers["Cookie"] = "security=true";
                html = webClient.DownloadString(url);
            }

            html = html.Replace("<span class=\"num_sorteio\"><ul>", "");
            html = html.Replace("</ul></span>", "");
            html = html.Replace("</li>", "");

            string[] vet = Regex.Split(html, "<li>");
            List<int> ResultadoMega = new List<int>();

            ResultadoMega.Add(int.Parse(vet[1]));
            ResultadoMega.Add(int.Parse(vet[2]));
            ResultadoMega.Add(int.Parse(vet[3]));
            ResultadoMega.Add(int.Parse(vet[4]));
            ResultadoMega.Add(int.Parse(vet[5]));
            ResultadoMega.Add(int.Parse(vet[6].Substring(0, 2)));

            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("Concurso selecionado: " + numeroConcurso);
            Console.WriteLine("------------------------------------------------");

            Console.WriteLine("------------------------------------------------ Resultado da Mega Sena: ------------------------------------------------");

            ResultadoMega.OrderBy(x => x).ToList().ForEach(num =>
            {
                Console.WriteLine(num);
            });

            Console.ReadKey();
        }
    }
}
